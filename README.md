# Dotfiles manged by stow

Clone the Repository to the root of your home to .dotfiles folder and execute
stow:

```bash
stow --adopt -vt ~ *
```

## Install all dotfiles

To install/copy all dotfiles execute the install.sh script:

```bash
bash install.sh
```
