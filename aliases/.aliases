#!/bin/bash
# GW ALIAS FILE (2023)

alias zshconfig="nvim ~/.zshrc"

# ----------------------------------------------------------------------------
# GOTO IN CERTAIN DIRS
# ----------------------------------------------------------------------------
if [ -d $HOME/.bin ]; then
    alias bin='cd $HOME/.bin'
fi

if [ -d $HOME/Dokumente ]; then
    alias dok='cd $HOME/Dokumente'
fi

if [ -d $HOME/Documents ]; then
    alias dok='cd ~/Documents'
fi

if [ -d $HOME/Downloads ]; then
    alias dow='cd ~/Downloads'
fi

if [ -d $HOME/Dokumente/docker ]; then
    alias doc='cd ~/Dokumente/docker'
fi

if [ -d $HOME/Documents/docker ]; then
    alias doc='cd ~/Documents/docker'
fi

if [ -d $HOME/Documents ]; then
    alias dok='cd ~/Documents'
fi

if [ -d $HOME/Junk ]; then
    alias junk='cd ~/Junk'
fi

if [ -d $HOME/Music ]; then
    alias music='cd ~/Music'
fi

if [ -d $HOME/Musik ]; then
    alias mus='cd ~/Musik'
fi

if [ -d $HOME/Videos ]; then
    alias vid='cd ~/Videos'
fi

# ----------------------------------------------------------------------------
# DOCKER STUFF
# ----------------------------------------------------------------------------
alias dcclean='docker volume prune -f & docker network prune -f'
alias dcclear='docker volume prune -f & docker network prune -f & docker rmi $(docker images -qa)'
alias dcprune='docker system prune -f'
alias dcpsa='docker ps -a'

# ----------------------------------------------------------------------------
# CLI SHORTENERS
# ----------------------------------------------------------------------------
alias .='source'
alias del='rm -fr'
alias e="exit"
alias erase='shred -uvf'
alias f='find . -name '
alias fg='find | grep -i'
alias h='history'
alias hg='history | grep'
alias jena="curl -H 'Accept-Language: de' http://wttr.in/jena"
alias kps='killall pipenv shell'
alias lsd="ls -lah --group-directories-first"
alias proc='ps aux | grep '
alias rb="reboot"
alias sd="poweroff"
alias t="tmux"
alias tt="trash"
alias v="nvim"
alias vim="nvim"
alias vimdiff="nvim -d"
alias zshconfig="nvim ~/.zshrc"

# ----------------------------------------------------------------------------
# VimWiki
# ----------------------------------------------------------------------------
if [ -f "$HOME/Dokumente/develop/vimwiki/index.md" ]; then
    alias wiki="nvim ~/Dokumente/develop/vimwiki/index.md"
fi

if [ -f "$HOME/Documents/develop/vimwiki/index.md" ]; then
    alias wiki="nvim ~/Documents/develop/vimwiki/index.md"
fi

alias x='startx'

# ----------------------------------------------------------------------------
# PYTHON (DJANGO UVICORN) STUFF
# ----------------------------------------------------------------------------
alias pin='touch __init__.py'
alias pipi='pip install'
alias pipir='pip install -r requirements.txt'
alias pipup='pip install --upgrade pip'
alias pm='python3 manage.py'
alias pmm='python3 manage.py migrate'
alias pmmm='python3 manage.py makemigrations'
alias pmr='python3 manage.py runserver'
alias ptag='ctags -R --exclude=.venv'
alias pv='python3 -m venv .venv'
alias pvsv='python3 -m venv .venv && source .venv/bin/activate'
alias pybug='python3 -m pdb'
alias python='/usr/bin/python3'
alias sv='source .venv/bin/activate'
alias uc='uvicorn main:app --reload'
alias uva='uvicorn app.main:app --reload'

# ----------------------------------------------------------------------------
# HTML WEBDEV
# ----------------------------------------------------------------------------
alias lr='livereload -p8000'

# ----------------------------------------------------------------------------
# Internet Radio
# ----------------------------------------------------------------------------
alias mus_bach='mpv http://stream.klassikradio.de/purebach/mp3-128/'
alias mus_barock='mpv http://stream.klassikradio.de/barock/mp3-192'
alias mus_beethoven='mpv http://stream.klassikradio.de/beethoven/mp3-128/'
alias mus_classique='mpv https://radioclassique.ice.infomaniak.ch/radioclassique-high.mp3'
alias mus_coderadio='mpv https://coderadio-relay-blr.freecodecamp.org/radio/8010/radio.mp3'
alias mus_fm4='mpv https://orf-live.ors-shoutcast.at/fm4-q2a'
alias mus_games='mpv http://stream.klassikradio.de/games/mp3-128/'
alias mus_jazz='mpv https://air.jazz.w3.at/jazzw3.ogg'
alias mus_mdr_klassik='mpv http://avw.mdr.de/streams/284350-0_aac_high.m3u'
alias mus_mdr_kultur='mpv http://avw.mdr.de/streams/284310-0_mp3_high.m3u'
alias mus_mozart='mpv http://stream.klassikradio.de/mozart/mp3-128/vtuner/'
alias mus_oper='mpv http://stream.klassikradio.de/opera/mp3-128/'
alias mus_paradise='mpv http://stream-uk1.radioparadise.com/aac-320'
alias mus_paradise_mellow='mpv http://stream.radioparadise.com/mellow-320'
alias mus_paradise_rock='mpv http://stream.radioparadise.com/rock-320'
alias mus_radioeins='mpv http://www.radioeins.de/live.m3u'
alias mus_rock_antenne_70er='mpv http://stream.rockantenne.de/70er-rock/stream/mp3'
alias mus_rock_antenne_80er='mpv http://stream.rockantenne.de/80er-rock/stream/mp3'
alias mus_rock_antenne_alternative='mpv https://stream.rockantenne.de/alternative/stream/mp3'
alias mus_rock_antenne_grunge='mpv https://stream.rockantenne.de/grunge/stream/mp3'
alias mus_rock_antenne_heavy_metal='mpv https://stream.rockantenne.de/heavy-metal/stream/mp3'
