".............................................................................
" Neovim Config Gunther Weissenbaeck (2023)
" .............................................................................

" Automatic reloading of .vimrc
autocmd! bufwritepost ~/.config/nvim/init.vim source %

" autowrite
set autowrite

call plug#begin('~/.local/share/nvim/plugged')


" .............................................................................
" Common vim pluggins
" .............................................................................
Plug 'Asheq/close-buffers.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'machakann/vim-highlightedyank'
Plug 'mhinz/vim-startify'
Plug 'sbdchd/neoformat'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'terryma/vim-multiple-cursors'
Plug 'tmhedberg/SimpylFold'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'vimwiki/vimwiki'
Plug 'dhruvasagar/vim-table-mode'
Plug 'rbong/vim-flog'


" .............................................................................
" Code Completion, Tags and Snippets
" .............................................................................
" Plug 'vim-scripts/taglist.vim'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'ycm-core/YouCompleteMe'


" .............................................................................
" Code Completion, Tags and Snippets
" .............................................................................
Plug 'tweekmonster/django-plus.vim'


" .............................................................................
" Themes and Colors
" .............................................................................
" Plug 'folke/tokyonight.nvim'
" Plug 'joshdick/onedark.vim'
" Plug 'morhetz/gruvbox'
Plug 'nordtheme/vim'
Plug 'juanedi/predawn.vim'


" .............................................................................
" Airline Themes and Colors
" .............................................................................
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'


" .............................................................................
" Python Plugins
" .............................................................................
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" Plug 'davidhalter/jedi-vim'
" Plug 'zchee/deoplete-jedi'
Plug 'ambv/black'
Plug 'neomake/neomake'
Plug 'python-mode/python-mode'


" .............................................................................
" Go-Lang Plugins
" .............................................................................
" Plug 'fatih/vim-go'


call plug#end()


" .............................................................................
" Neovim stuff
" .............................................................................
" colorscheme gruvbo
" colorscheme nord
colorscheme predawn


" .............................................................................
" Better copy & paste
" When you want to paste large blocks of code into vim, press F2 before you
" paste. At the bottom you should see ``-- INSERT (paste) --``.
" .............................................................................
set pastetoggle=<F2>
set clipboard=unnamed


" .............................................................................
" Rebind <Leader> key to Space
" .............................................................................
let mapleader = " "


" .............................................................................
" Bind nohl
" Removes highlight of your last search
" ``<C>`` stands for ``CTRL`` and therefore ``<C-n>`` stands for ``CTRL+n``
" .............................................................................
noremap <C-x> :nohl<CR>
vnoremap <C-x> :nohl<CR>
inoremap <C-x> :nohl<CR>


" .............................................................................
" Quicksave command
" .............................................................................
noremap <C-Z> :update<CR>
vnoremap <C-Z> <C-C>:update<CR>
inoremap <C-Z> <C-O>:update<CR>


" .............................................................................
" Quick quit command
" .............................................................................
noremap <Leader>e :quit<CR>  " Quit current window
noremap <Leader>E :qa!<CR>   " Quit all windows
noremap <Leader>q <esc><c-w>j<esc>:q<CR>


" .............................................................................
" Bind Ctrl+<movement> keys to move around the windows, instead of using
" Ctrl+w + <movement>
" Every unnecessary keystroke that can be saved is good for your health :)
" .............................................................................
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h


" .............................................................................
" Easier moving between tabs
" .............................................................................
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>
map <Leader>N <esc>:tabnew<CR>


" .............................................................................
" Easier moving between buffers
" .............................................................................
map <Leader>k <esc>:bn<CR>
map <Leader>j <esc>:bp<CR>

" .............................................................................
" Map sort function to a key
" .............................................................................
vnoremap <Leader>s :sort<CR>


" .............................................................................
" Easier moving of code blocks
" Try to go into visual mode (v), thenselect several lines of code here and
" then press ``>`` several times.
" .............................................................................
vnoremap < <gv  " better indentation
vnoremap > >gv  " better indentation


" Enable syntax highlighting
" You need to reload this file for the change to apply
" .............................................................................
filetype off
filetype plugin indent on
syntax on


" .............................................................................
" Showing line numbers and length
" .............................................................................
" set nowrap  " don't automatically wrap on load
set autoindent
set colorcolumn=80
set fo+=t   " automatically wrap text when typing
set fo-=t   " don't automatically wrap text when typing
set formatoptions=tacqw
set number  " show line numbers
set relativenumber " show relative numbers in present line
set tw=79   " width of document (used by gd)
set wrapmargin=0

highlight ColorColumn ctermbg=233


" .............................................................................
" Easier formatting of paragraphs
" .............................................................................
vmap Q gq
nmap Q gqap


" .............................................................................
" Make search case insensitive
" .............................................................................
set hlsearch
set incsearch
set ignorecase
set smartcase


" .............................................................................
" Real programmers don't use TABs but spaces
" .............................................................................
set expandtab
set shiftround
set shiftwidth=4
set smartindent
set softtabstop=4
set tabstop=4


" .............................................................................
" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
" .............................................................................
set nobackup
set nowritebackup
set noswapfile
set scrolloff=8


" .............................................................................
" Remember last position in file
" .............................................................................
if has("autocmd")
    au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
                \| exe "normal! g'\"" | endif
endif


" .............................................................................
" Toggle spellchecking
" .............................................................................
function! ToggleSpellCheck()
    set spell!
    if &spell
        echo "Spellcheck ON"
    else
        echo "Spellcheck OFF"
    endif
endfunction
nnoremap <silent> <Leader>S :call ToggleSpellCheck()<CR>


" .............................................................................
" Running a macro
" .............................................................................
nnoremap <Space> @q


" .............................................................................
" Help text window on the right pane
" .............................................................................
autocmd FileType help wincmd L


" .............................................................................
" Vsplit to the right panel | Split below
" .............................................................................
:set splitright
:set splitbelow


" .............................................................................
" NERDTree
" .............................................................................
" let g:NERDTreeDirArrowExpandable="+"
" let g:NERDTreeDirArrowCollapsible="~"
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
" map <C-r> :NERDTreeToggle<CR>

" slent! nmap <C-p> :NERDTreeToggle<CR>
silent! nmap <F2> :NERDTreeToggle<CR>
silent! map <F3> :NERDTreeFind<CR>

let g:NERDTreeMapActivateNode="<F3>"
let g:NERDTreeMapPreview="<F4>"


" .............................................................................
" NERDCOMMENTER
" add space after comment
" .............................................................................
let g:NERDSpaceDelims = 4


" .............................................................................
" Close-Buffers Shortcut to delete hidden buffers
" .............................................................................
map <Leader>bh <esc>:Bdelete hidden<CR>


" .............................................................................
" Vim airline themes
" .............................................................................
let g:airline_theme='ayu_mirage'
" let g:airline_theme='gruvbox'
let g:airline_powerline_fonts = 1
" let g:airline_theme='google_dark'

" .............................................................................
" Vimwiki settings
" .............................................................................
let g:vimwiki_list = [{'path': '~/Dokumente/develop/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.mkd': 'markdown', '.wiki': 'media'}


" .............................................................................
" Python Jedi settings
" .............................................................................
" let g:deoplete#enable_at_startup = 1
" autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" " Enable alignment
" let g:neoformat_basic_format_align = 1

" " Enable tab to space conversion
" let g:neoformat_basic_format_retab = 1

" " Enable trimmming of trailing whitespace
" let g:neoformat_basic_format_trim = 1

" let g:neomake_python_enabled_makers = ['pylint']

" call neomake#configure#automakehortcuts = 1
" hi HighlightedyankRegion cterm=reverse gui=reverse

" " set highlight duration time to 1000 ms, i.e., 1 second
" let g:highlightedyank_highlight_duration = 1000


" .............................................................................
" Python-settings
" .............................................................................
autocmd FileType python set sw=4
autocmd FileType python set ts=4
autocmd FileType python set sts=4

" .............................................................................
" Python-mode-settings
" .............................................................................
" let g:pymode_syntax_builtin_funcs = 0
" let g:pymode_syntax_builtin_objs = 0
let g:pymode_options = 1
let g:pymode_options_colorcolumn = 1
let g:pymode_quickfix_maxheight = 6
let g:pymode_quickfix_minheight = 3
let g:pymode_syntax = 1

" .............................................................................
" Pymode-mappings
" .............................................................................
" map <Leader>b Oimport ipdb; ipdb.set_trace() # BREAKPOINT<C-c>
" map <Leader>g :call RopeGotoDefinition()<CR>
map <Leader>B <esc>:Black<CR>
map <Leader>l <esc>:PymodeLintAuto<CR>
map <Leader>p <esc>:PymodeRun<CR>

" .............................................................................
" Python-mode Rope settings
" .............................................................................
let g:pymode_rope = 1
let g:pymode_rope_autoimport = 1
let g:pymode_rope_goto_def_newwin = "vnew"
let g:pymode_rope_organize_imports_bind = '<C-c>ro'


" .............................................................................
" YouCompleteMe
" .............................................................................
" let g:ycm_collect_identifiers_from_tags_files = 1 " Let YCM read tags from 
" Ctags file
" let g:ycm_complete_in_comments = 1 " Completion in comments
" let g:ycm_complete_in_strings = 1 " Completion in string
" let g:ycm_key_list_stop_completion = ['<C-s>']
" let g:ycm_seed_identifiers_with_syntax = 1 " Completion for programming language's keyword
" let g:ycm_use_ultisnips_completer = 1 " Default 1, just ensure


" .............................................................................
" UltiSnippets
" .............................................................................
" let g:UltiSnipsExpandTrigger       = "<tab>"
" let g:UltiSnipsJumpBackwardTrigger = "<c-p>"
" let g:UltiSnipsJumpForwardTrigger  = "<c-j>"
" let g:UltiSnipsListSnippeto        = "<c-k>" "List possible snippets based on current file


" .............................................................................
" Vim-Go
" .............................................................................
map <Leader>g <esc>:GoRun<CR>
map <Leader>f <esc>:GoFmt<CR>


" .............................................................................
" Table Mode
" .............................................................................
" To start using the plugin in the on-the-fly mode use :TableModeToggle mapped 
" to <Leader>tm by default (which means \ t m if you didn't override the by 
" :let mapleader = ',' to have , t m).

" Tip: You can use the following to quickly enable / disable table mode in 
" insert mode by using || or __:

function! s:isAtStartOfLine(mapping)
  let text_before_cursor = getline('.')[0 : col('.')-1]
  let mapping_pattern = '\V' . escape(a:mapping, '\')
  let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', '', ''), '\')
  return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
endfunction

inoreabbrev <expr> <bar><bar>
          \ <SID>isAtStartOfLine('\|\|') ?
          \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
inoreabbrev <expr> __
          \ <SID>isAtStartOfLine('__') ?
          \ '<c-o>:silent! TableModeDisable<cr>' : '__'

