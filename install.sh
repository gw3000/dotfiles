#!/bin/bash

stow -t ~ aliases
stow -t ~ git
stow -t ~ i3
stow -t ~ tmux
stow -t ~ top
stow -t ~ vim
stow -t ~ zsh
